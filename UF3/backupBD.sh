'''Creeu un script anomenat backupBD.sh que faci un backup de les bases de dades d’un usuari passat com argument.
Per cada base de dades d’aquest usuari, es generarà un fitxer .sql amb nom de  l’usuari, el nom de la base de dades 
i la data de quan es fa el backup, seguint el següent exemple: si l’usuari passat com argument es diu jordi i aquest 
usuari té les bases de dades videoclub i carreteres, els backups generats es diran jordi_videoclub_20240322.sql i 
jordi_carreteres_20240322.sql.  El backup serà amb inserts i cal incloure l’ordre de creació de la base de dades'''

