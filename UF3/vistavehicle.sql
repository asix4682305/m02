--a232319nm
--Noah Martos Teruel

--Creeu la vista vehicle on sortiran tots els vehicles.
--matricula, marca, model, tipus i etiqueta.
--etiqueta: 0 emissions-> cotxe electric de bateria / hibrid endollable.
--eco: cotxe híbrid no endollable / gas natural / gas liquat
--C: benzina gener 2006 / diesel setembre 2015
--B: benzina 2001 / diesel 2006

SELECT matricula,
        marca,
        model,
        tipus,
        CASE
            WHEN lower(tipus)='elèctric de bateria'
                OR lower(tipus)='hibrid'
                AND lower(subtipus)='endollable'
            THEN '0 emissions'
            WHEN lower(tipus)='hibrid'
                AND lower (subtipus)='no endollable'
                OR lower(subtipus)='gas natural'
                OR lower(subtipus)='gas liquat'
            THEN 'eco'
            WHEN lower(subtipus)='benzina'
                AND data < 2006
                OR lower(subtipus)='diesel'
                AND data < 2015
            THEN 'C'
            WHEN lower(subtipus)='benzina'
                AND data < 2001
                OR lower(tipus)='diesel'
                AND data < 2006
            THEN 'B'
            ELSE 'No té etiqueta'
            END AS etiqueta
        FROM cotxe;