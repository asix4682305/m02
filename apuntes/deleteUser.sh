#! /bin/bash
#Noah Martos Teruel

#---------------------------------------------------
if [ $# -ne 0 ]
then
    echo "ERR! el programa no requereix arguments"
    echo "Usage: $0"
    exit 1
fi

deleted_users=0

for user in $(psql template1 -t \
            -c SELECT rolname
            FROM pg_roles
            WHERE rolvaliduntil < CURRENT_TIMESTAMP;)
    do
        psql template1 -c "DROP ROLE $user;"
        if [ $? -eq 0 ]
            then
                echo "Usuari $user eliminat."
        fi
        ((usuaris_borrats++))
    done
exit 0