Noah Martos Teruel

SUBCONSULTAS

Consultas dentro de otras consultas.

"¿Quién tiene un salario superior a Jones?"

    select ename
    from emp 2975
    where sal >
        (select sal
            from emp
                where
                    lower(ename)='jones');

La subconsulta es llegida abans.

Subconsulta:
-Siempre encerrada entre parentesis.
-Debe aparecer a la derecha del operador.
-No utilizar una cláusula order by en una subconsulta.

Tipos:

-Monoregistro: Un único valor

    Operaciones relacionales (de comparación):
        =, >, >=, <, <=, !=

    select ename, job
    from emp
    where sal< 
        (select sal
        from emp
        where lower(ename)='james');

    "Muestrame el nombre y trabajo de los empleados que cobran menos que james."

    select ename, job, sal
    from emp
    where sal =
        (select min(sal)
        from emp);

    "Muestrame el nombre, trabajo y salario de los/el empleado/s con el sueldo menor."

    Si fuera 'mayor a 2000', podriamos hacer having ...>2000, pero como no conocemos
    el valor 'salario mínimo', tenemos que hacer subconsulta.

    -Subconsulta en el having:

    select deptno, min(sal)
    from emp
    group by deptno
    having min(sal)>
        (select min(sal)
        from emp
        where deptno=20);

    "Mostra el departament i salari mínim dels empleats als departaments on el
    salari mínim es mayor al salari mínim del empleats del departament 20."

    ERROR en esta sentencia:

    select empno, ename
    from emp
    where sal =
        (select min(sal)
        from emp
        group by deptno);

    Està malament perque retorna més d'una fila, ja que l'operador és per a consultes
    multiregistre. Una consulta amb funció de grup retorna n (tants com valors diferents)
    files. L'operador '=' només pot fer-se servir amb un valor, no amb n.

-Multiregistro: Más de un resultado.

    Comparadores multiregistro:
        IN (igual a los valores en la lista)

            where lower(job) (NOT) IN ('clerk', 'king', ...)

        op. rel. ANY (compara valores con cada valor devuelto por la subconsulta)

            where sal > any 
                (select sal
                from emp
                where upper(job)='CLERK');

            "Muestra el salario de los empleados cuyo salario es superior a cualquier
            clerk"

        op. rel. ALL (compara los valores con cada uno de los valores devueltos por la subconsulta)

            where sal > all 
                (select sal
                from emp
                where upper(job)='CLERK');

            "Muestra el salario de los empleados cuyo sueldo es superior a todos los
            clerk"


-Multicolumna: Más de una columna.

IN --> = ANY
NOT IN --> !=ALL

[EJERCICIOS]
1. Mostra els departaments que no tenen assignat cap empleats

select *
from dept
where deptno != ALL
    (select deptno from emp);

No funciona si hay un nulo en la tabla emp, ja que el null no es pot operar:
    el descartem o el transformem, en aquest cas, és millor descartar-ho
    pq no ho necessitem. Con el in/all si funciona, pq con que haya una sola
    coincidencia ya funciona, no repasa todas las coincidencias.

Mostrar els empleats que treballen al mateix departament i del mateix job.

    select ename, deptno, job
    from emp e
    where (deptno, job) IN (select deptno,job
        from emp
        where empno!=e.empno)
    order by 2, 3;

select nombre
from repventa
.......... min
union
select nombre
from repventa;

union-> unir elements comuns
-no elements reptits
-no existeix l'ordre 
-han de ser del mateix tipus

intersect-> 