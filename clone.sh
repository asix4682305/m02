if [ $# -eq 0 ]
then
	echo "Err! n args invàlid"
	echo "Usage: $0 repositori/s"
	exit 1
fi

for repo in $*
do
	if [ -d $repo ]
	then
		rm -rf $repo
		echo "Repositori $repo eliminat"
		git clone https://gitlab.com/asix4682305/$repo
		echo "Repositori $repo creat"
	else
		echo "Err! repositori $repo no existeix" >/dev/null
	fi
done
exit 0

