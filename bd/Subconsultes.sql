--a232319nm
--Noah Martos Teruel
--Subconsultes

--1. Mostra els departaments que no tenen assignat cap empleats.
select *
from dept
where deptno != ALL
    (select deptno from emp
    where deptno is not null);

select *
from dept
where deptno NOT IN
    (select deptno from emp);

--2. Mostrar el nom dels empleats que són cap.

select ename
from emp
where empno in 
    (select mgr
    from emp);

select distinct cap.ename
from emp join emp cap
on emp.mgr=cap.empno;

--Si son los que no son cap

select ename
from emp
where empno not in 
    (select mgr
    from emp
    WHERE mgr IS NOT NULL);