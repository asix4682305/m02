--a232319nm
--Noah Martos Teruel

--0. Mostrar el nombre y el puesto de los que son jefe (ya está hecho
--con self join, ahora con subconsultas)

select nombre, puesto
from repventa
where repcod in (
    select jefe from repventa
);

--1. Obtener una lista de los representantes cuyas cuotas son iguales ó
--superiores al objetivo de la oficina de Atlanta.

select *
from repventa
where cuota >= (
    select objetivo
    from oficina
    where lower(ciudad)='atlanta'
);

--2. Obtener una lista de todos los clientes (nombre) que fueron
--contactados por primera vez por Bill Adams.

select nombre
from cliente
where repcod = (
    select repcod
    from repventa
    where lower(nombre)='bill adams'
);

--3. Obtener una lista de todos los productos del fabricante ACI cuyas
--existencias superan a las existencias del producto 41004 del mismo
--fabricante.

select *
from producto
where lower(fabcod)='aci' and exist > (
    select exist
    from producto
    where prodcod='41004' and lower(fabcod)='aci'
);

--4. Obtener una lista de los representantes que trabajan en las oficinas
--que han logrado superar su objetivo de ventas.

select *
from repventa
where ofinum in (
    select ofinum
    from oficina
    where ventas > objetivo
);

--5. Obtener una lista de los representantes que no trabajan en las
--oficinas dirigidas por Larry Fitch.

select *
from repventa
where ofinum != (
    select ofinum
    from repventa
    where lower(nombre)='larry fitch'
);

--6. Obtener una lista de todos los clientes que han solicitado pedidos
--del fabricante ACI entre enero y junio de 2003.

select *
from cliente join pedido
    on pedido.cliecod=cliente.cliecod
where pednum in (
    select pednum
    from pedido
    where lower(fabcod)='aci' 
    and to_char(fecha, 'yyyy') = '2003' 
    and to_char(fecha,'mm') between '01' and '06'
);

--7. Obtener una lista de los productos de los que se ha tomado un pedido
--de 150 euros ó mas.

select *
from producto
where fabcod||prodcod in (select fabcod||prodcod
    from pedido
    where importe>150
    );

--8. Obtener una lista de los clientes contactados por Sue Smith que no
--han solicitado pedidos con importes superiores a 18 euros.

select *
from cliente join repventa
    on cliente.repcod=repventa.repcod
join pedido
    on pedido.cliecod=cliente.cliecod
where lower(repventa.nombre) = 'sue smith'
    and importe in (
        select importe
        from pedido
        where importe < 18
    );

--9. Obtener una lista de las oficinas en donde haya algún representante
--cuya cuota sea más del 55% del objetivo de la oficina. Para comprobar vuestro
--ejercicio, haced una Consulta previa cuyo resultado valide el ejercicio.

select *
from oficina join repventa
    on oficina.ofinum=repventa.ofinum
where cuota>0.55*objetivo;

select *
from oficina
where ofinum in ( select r.ofinum
    from oficina o join repventa r
        on r.ofinum=o.ofinum
    where cuota > objetivo*0.55 );

--10. Obtener una lista de los representantes que han tomado algún pedido
--cuyo importe sea más del 10% de de su cuota.

select *
from repventa
where repcod in (
    select repventa.repcod
    from pedido join repventa
        on pedido.repcod=repventa.repcod
    where importe>0.1*cuota
);

--11. Obtener una lista de las oficinas en las cuales el total de ventas
--de sus representantes han alcanzado un importe de ventas que supera el
--50% del objetivo de la oficina. Mostrar también el objetivo de cada
--oficina (suponed que el campo ventas de oficina no existe).

select oficina.ofinum, 
    objetivo, 
    sum(repventa.ventas) "ventas"
from oficina join repventa
    on oficina.ofinum=repventa.ofinum
group by oficina.ofinum, objetivo
having sum(repventa.ventas) > 0.5*objetivo;

--12. ¿Cuál es la descripción del primer producto solicitado en un pedido?

select descrip
from producto
where (fabcod, prodcod) = (
    select fabcod, prodcod
    from pedido
    order by fecha
    limit 1
);

--13. ¿Qué representante tiene el mejor porcentaje de ventas?

select *
from repventa
where ventas/cuota = (
    select max(ventas/cuota)
    from repventa
);

--14. ¿Qué representante tiene el peor porcentaje de ventas?

select *
from repventa
where ventas/cuota = (
    select min(ventas/cuota)
    from repventa
);

--15. ¿Qué producto (Descripción) tiene más pedidos?

select descrip 
from producto join pedido
    on producto.prodcod=pedido.prodcod
where prodcod = (
    select prodcod
    from pedido
    
)

--16. ¿Qué producto se ha vendido más?

select producto.prodcod, producto.fabcod, descrip, sum (cant) "cantidad"
from producto join pedido
    on producto.prodcod=pedido.prodcod
    and producto.fabcod=pedido.fabcod
group by producto.prodcod, producto.fabcod, descrip, cant
order by sum(cant) desc 
limit 1;