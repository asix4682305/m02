--a232319nm
--Noah Martos Teruel

--[FUNCIONS DE GRUP]
--41. Calcular el salari total mensual.
select sum(salari) "salari mensual total"
from empleat;
--42. Per a cada departament, mostreu el
--nom de departament, localitat, n d'empleats
-- assignats i acumulat de salaris.

select dname "Nom dept.", loc "localitat", count(*) "empleats", sum(sal) "salari total"
from dept join emp
    on dept.deptno=emp.deptno
group by dept.deptno;

--43. Mostreu per cada departament l'acumulat 
--de salaris, l'acumulat de comissions i la
--suma de tots dos.

select dname "nom dept.", 
    sum(coalesce(sal,0)) "sal.", 
    sum(coalesce(comm,0)) "comm.", 
    sum(coalesce(sal,0))+sum(coalesce(comm,0)) "sal+comm"
from emp right join dept on dept.deptno=emp.deptno
group by dept.deptno;

--44. Inseriu un nou empleat sense assignar-li
--ocupació. A continuació mostreu el salari, mínim
--i màxim dels empleats, agrupats per ocupació.

insert into emp
values (1234, 'Noah', null, null, '20-12-2005', 5000, 4000, 10);

select coalesce(job,'---') "trabajo", 
    min(sal), 
    max(sal)
from emp
group by job;

--45. Mostreu el salari mínim, maxim i mig dels empleats
--agrupats per feina, pero nomes d'aquells que la mitjana
--sigui superior a 4000.

select coalesce(job,'sense asignar') "trabajo", 
    min(sal), 
    max(sal), 
    round(avg(sal),2)
from emp
group by job
having avg(sal)>4000;

--46. Mostreu el codi i el nom dels departaments que tinguin
--més de tres empleats assignats.

select dept.deptno, dname, count(*) "n empleats"
from dept left join emp
on dept.deptno=emp.deptno
group by dept.deptno
having count(*)>3;

--[EXERCICIS SENSE SUBCONSULTES]

--54. Mostreu el treball, el nom i el salari dels empleats ordenats pel
--tipus de treball i per salari descendent.

select job, ename, sal
from emp
order by job, sal desc;

--55. Mostreu el nom de cada empleat, i el nombre i nom del seu cap.

select c.ename 'empleat', e.empno 'codi cap', e.ename 'cap'
from emp e join emp c
on e.empno=c.mgr;

--56. Mostreu dels empleats que son analyst el nom de l'empleat, la seva data d'alta
--en l'empresa, anys, mesos i dies que porta contractat (no podeu utilitzar cap funció)
--dels empleats que son analyst.

select ename "Nom", 
    hiredate "Dada contractacio", 
    round((current_date - hiredate)%365%30, 1) "Dies",
    round((current_date - hiredate)%365/30, 1) "Messos",
    round((current_date - hiredate)/365, 1) "Anys"
from emp;

--57. Trobeu el salari mitjà, d'aquell empleats el treball sigui el d'analista.

select round(avg(sal),2), ename
from emp
where upper(job)='ANALYST'
group by ename;

--58. Trobeu el salari més alt, el més baix i la diferencia entre tots dos.

select max(sal) "salari maxim", min(sal) "salari minim", max(sal)-min(sal) "diferencia"
from emp
group by empno;

--59. Trobeu el nobre de treball diferents que hi ha al deprtament 30.

select count(distinct job) "Treballs diferents (dept. 30)"
from emp join dept
on emp.deptno=dept.deptno
group by dept.deptno
having dept.deptno=30;

--60. Mostreu el nom de l'empleat, el seu treball, el nom i el codi del departament en el que reballa

select ename "Empleado", 
    coalesce(job,'no asignat') "Trabajo",
    dname "Departament",
    dept.deptno "N Departament"
from emp left join dept
on emp.deptno=dept.deptno;

--61. Mostreu el nom, el treball i el salari de tots els empleats que tenen un salari superior al
--salari més baix del departament 30.
