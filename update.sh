if [ $# -eq 0 ]
then
	echo "Err! n args invalid"
	echo "Usage: $0 repo"
	exit 1
fi

for repo in $*
do
	if [ -d $repo ]
	then
		cd $repo
		git add .
		git commit -m "msg"
		git push
		echo "Update realizada"
		cd ..
	else
		echo "El repositori no existeix"
	fi
done
exit 0
